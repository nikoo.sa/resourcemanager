package JFZ.uni.TA;

import JFZ.uni.TA.Commons.AbstractClient;
import JFZ.uni.TA.Commons.GameServer;
import JFZ.uni.TA.Commons.TileType;

import java.awt.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


public class Client extends AbstractClient {

    boolean[][] visited;    //for optimizing moveRand
    boolean knowThePath;    //if there is any answer by bfs
    Point[][] prevs;    //in the path made by bfs
    Stack<Point> path;  //made by bfs
    int[] deltaX = {1, -1, 0, 0};   //used for iterating neighbours
    int[] deltaY = {0, 0, 1, -1};

    public Client(GameServer gameServer) {
        super(gameServer);
        visited = new boolean[gameServer.getMapSize()][gameServer.getMapSize()];
        prevs = new Point[visited.length][visited.length];
        path = new Stack<Point>();
        for (int i = 0; i < visited.length; i++)
            for (int j = 0; j < visited.length; j++)
                visited[i][j]=false;
    }
    public Point continuePath() {   //move on the path made by bfs
        return path.pop();
    }

    public void makePath(Point s, Point g){ //get the path from prevs
        while (!g.equals(s)){
            path.push(g);
            g = prevs[g.x][g.y];
        }
    }

    public Point lampNeighbour(Point currentP) {
        Point pLamp = new Point();
        for (int i = 0; i < 4; i++) {
            pLamp.setLocation(currentP.x + deltaX[i], currentP.y + deltaY[i]);
            if (gameServer.typeOfTile(pLamp.x, pLamp.y) == TileType.LAMP && !visited[pLamp.x][pLamp.y]) return pLamp;
        }
        return null;
    }
    public Point DoorNeighbour(Point currentP) {
        Point pDoor = new Point();
        for (int i = 0; i < 4; i++) {
            pDoor.setLocation(currentP.x + deltaX[i], currentP.y + deltaY[i]);
            if (gameServer.typeOfTile(pDoor.x, pDoor.y) == TileType.DOOR) return pDoor;
        }
        return lampNeighbour(currentP);
    }
    public Point moveRand(Point currentP) {
        Point newP;
        int i;
        newP = DoorNeighbour(currentP); //in order to optimizing random move, first of all we check whether there is any door in neighbour or not, if not we check unvisited lamp in neighbour and after all we go to choosing random
        if (newP != null) return newP;
        newP = new Point();
        for (i = 0; i < 4; i++) {
            newP.setLocation(currentP.x + deltaX[i], currentP.y + deltaY[i]);
            if (gameServer.typeOfTile(newP.x, newP.y) != TileType.BLOCK && !visited[newP.x][newP.y]) break;
        }
        if (i == 4){    //if there is no unvisited and unblocked neighbour left
            int j;
            for (j = 0; j < 4; j++) {
                newP.setLocation(currentP.x + deltaX[j], currentP.y + deltaY[j]);
                if (gameServer.typeOfTile(newP.x, newP.y) != TileType.BLOCK) break;
            }
            /*if (j ==4) {System.out.println("start is surrounded by blocks!");
            return currentP;}*/
        }
        return newP;
    }
    public void doTurn(){
        Point newPos;
        Point currentPos = new Point(gameServer.getCurrentX(), gameServer.getCurrentY());
        if (knowThePath) newPos = continuePath();
        else {
            if (gameServer.typeOfTile(currentPos.x, currentPos.y) == TileType.LAMP) {
                Point bfsRes = bfs(currentPos);
                if (bfsRes == null) newPos = moveRand(currentPos);
                else {
                    makePath(currentPos, bfsRes);
                    knowThePath = true;
                    newPos = continuePath();
                }
            } else {
                newPos = moveRand(currentPos);
            }
        }
        move(newPos.x, newPos.y);
        visited[newPos.x][newPos.y] = true;
    }
    public Point bfs(Point start) {
        Point door = new Point();
        Queue<Point> queue = new LinkedList<Point>();
        boolean finish = false;
        boolean [][] visBfs = new boolean[visited.length][visited.length];
        for (int i = 0; i < visited.length; i++)
            for (int j = 0; j < visited.length; j++)
                visBfs[i][j]=false;
        queue.add(start);
        while(!queue.isEmpty() && !finish){
            start = queue.remove();
            visBfs[start.x][start.y] = true;
            Point adj;
            for (int i = 0; i < 4; i++) {
                adj = new Point(start.x + deltaX[i], start.y + deltaY[i]);
                if(!visBfs[adj.x][adj.y] && gameServer.typeOfTile(adj.x, adj.y) != TileType.BLOCK){
                    queue.add(adj);
                    prevs[adj.x][adj.y] = start;
                    if(gameServer.typeOfTile(adj.x, adj.y) == TileType.DOOR){
                        finish = true;
                        door = adj;
                    }
                }
            }
        }
        return (finish ? door : null);
    }
}
